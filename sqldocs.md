# Tabelas

CREATE TABLE tipos_material (
  id SERIAL not null primary key,
  nome text UNIQUE,
  status char(1) default 'S'
);

CREATE TABLE materiais (
  id SERIAL not null primary key,
  nome text UNIQUE,
  descricao text,
  reciclavel boolean default true,
  status char(1) default 'S',
  id_tipo integer not null REFERENCES tipos_material(id)
);

CREATE TABLE fotos_material (
  id SERIAL not null,
  id_material integer not null REFERENCES materiais(id),
  url text,
  usavel char(1) default 'S',
  PRIMARY KEY (id, id_material)
);

CREATE TABLE usuarios (
  id SERIAL not null,
  login text not null unique,
  senha text not null,
  nome text,
  status char(1) default 'S',
  PRIMARY KEY (id)
);

CREATE TABLE pontuacoes (
  id SERIAL not null,
  valor integer not null,
  id_usuario integer not null REFERENCES usuarios(id),
  id_rota integer not null REFERENCES rotas(id),
  data date,
  status char(1) default 'S',
  descricao text,
  PRIMARY KEY (id)
);

CREATE TABLE metas_pontuacao (
  id SERIAL not null,
  descricao text,
  pontuacao integer not null,
  id_rota integer not null REFERENCES rotas(id),
  data date,
  status char(1) default 'S',
  PRIMARY KEY (id)
);

CREATE TABLE estados (
  id SERIAL not null,
  nome text not null,
  nome_reduzido text not null,
  id_pais integer not null REFERENCES paises(id),
  status char(1) default 'S',
  PRIMARY KEY (id)
);

CREATE TABLE paises (
  id SERIAL not null,
  nome text not null unique,
  nome_reduzido text not null,
  status char(1) default 'S',
  PRIMARY KEY (id)
);

CREATE TABLE cidades (
  id SERIAL not null,
  nome text not null,
  id_estado integer not null REFERENCES estados(id),
  status char(1) default 'S',
  PRIMARY KEY (id)
);

CREATE TABLE bairros (
  id SERIAL not null,
  nome text not null,
  id_cidade integer not null REFERENCES cidades(id),
  status char(1) default 'S',
  PRIMARY KEY (id)
);

CREATE TABLE rotas (
  id SERIAL not null,
  dia_semana integer not null,
  id_bairro integer not null REFERENCES bairros(id),
  periodo char(1) not null,
  status char(1) default 'S',
  PRIMARY KEY (id)
);
