import { Client, Pool } from 'pg';
import { DATABASE_URL } from '../config';

export const query = (sql, values = []) => {
  const client = new Client({
    connectionString: DATABASE_URL,
    ssl: true,
  });

  client.connect();
  return client.query(sql, values)
    .then((res) => res.rows)
    .catch((err) => {
      return err.message;
    })
    .then((res) => {
      client.end();
      return res;
    });
};

export const poll = new Pool({
  connectionString: DATABASE_URL,
  ssl: true,
});
