import { query } from '../../database';

const getListMaterials = () => {
  return query(`
    select materiais.*,
           tipos_material.nome as nome_tipo_material
      from materiais inner join tipos_material on (materiais.id_tipo = tipos_material.id and tipos_material.status = 'S')
     where materiais.status = 'S'
     order by nome`)
    .then((retorno) => retorno.map((item) => ({ ...item, nomeTipoMaterial: item.nome_tipo_material, 'nome_tipo_material': undefined })));
};

const getInfoMaterial = (ctx) => {
  const { id } = ctx.params;
  return query('select * from materiais where id = $1', [id]).then((list) => {
    const item = list[0] || {};
    return query('select * from fotos_material where id_material = $1', [id]).then((fotos) => {
      return {
        ...item,
        fotos,
      };
    });
  });
};

const insertMaterial = (ctx) => {
  const { nome, descricao, idTipo } = ctx.data;
  return query('insert into materiais(nome, descricao, id_tipo) values($1, $2, $3)', [nome, descricao, idTipo]);
};

const deleteMaterial = (ctx) => {
  const { id } = ctx.params;
  return query('update materiais set status = $2 where id = $1', [id, 'N']);
};

const updateMaterial = (ctx) => {
  const { id, nome, descricao, idTipo } = ctx.data;
  return query('update materiais set nome = $1, descricao = $2, id_tipo = $3 where id = $4', [nome, descricao, idTipo, id]);
};

const insertMaterialType = (ctx) => {
  const { nome } = ctx.data;
  return query('insert into tipos_material(nome) values($1) ON CONFLICT (nome) DO UPDATE SET status = $2', [nome, 'S']);
};

const deleteMaterialType = (ctx) => {
  const { id } = ctx.params;
  return query('update tipos_material set status = $2 where id = $1', [id, 'N']);
};

const updateMaterialType = (ctx) => {
  const { id, nome } = ctx.data;
  return query('update tipos_material set nome = $1 where id = $2', [nome, id]);
};

const getListMaterialType = () => {
  return query(`
    select * from tipos_material where tipos_material.status = 'S'
  `);
};

const insertMaterialPhoto = (ctx) => {
  const { idMaterial, url } = ctx.data;
  return query('insert into fotos_material(id_material, url) values($1, $2)',[idMaterial, url]);
};

const updateMaterialPhoto = (ctx) => {
  const { id, idMaterial, url, status } = ctx.data;

  if ((status !== null) && (status !== undefined)) {
    return query('update fotos_material set status = $2 where id = $1', [id, status]);
  } else {
    return query('update fotos_material set id_material = $1, url = $2 where id = $3', [idMaterial, url, id]);
  }
};

const deleteMaterialPhoto = (ctx) => {
  const { id } = ctx.params;
  return query('update fotos_material set status = $2 where id = $1',[id, 'N']);
};

const getListPhotoMaterial = (ctx) => {
  const { id } = ctx.params;
  return query(`
    select * from fotos_material where status = 'S' and id_material = $1
  `,[id]);
};

const getPhotoMaterial = () => {
  return query(`
    select * from fotos_material where status = 'S'
  `,);
};


export default {
  insertMaterialType,
  deleteMaterialType,
  updateMaterialType,

  insertMaterial,
  deleteMaterial,
  updateMaterial,

  insertMaterialPhoto,
  updateMaterialPhoto,
  deleteMaterialPhoto,

  getListMaterials,
  getInfoMaterial,
  getListMaterialType,
  getListPhotoMaterial,

  getPhotoMaterial,
};
