// NOTE: Material

/**
* @api {get} /api/materials Buscar materiais cadastrados
* @apiName materials
* @apiGroup Material
*
* @apiSuccess {Object[]} materiais Lista de materiais.
*/

/**
* @api {put} /api/material Alterar material cadastrado
* @apiName putMaterial
* @apiGroup Material
*
* @apiParam {Number} id ID do material.
* @apiParam {String} nome Nome do material.
* @apiParam {String} descricao Descrição do material.
* @apiParam {Number} idTipo ID do tipo do material.
*/

/**
* @api {del} /api/material/:id Inativar material
* @apiName delMaterial
* @apiGroup Material
*
* @apiParam {Number} id ID do material.
*/

/**
* @api {post} /api/material Inserção de material
* @apiName postMaterial
* @apiGroup Material
*
* @apiParam {String} nome Nome do material.
* @apiParam {String} descricao Descrição do material.
* @apiParam {Number} idTipo ID do tipo do material.
*/


// NOTE: Tipo de material
/**
* @api {get} /api/typesMaterial Buscar tipos de materiais cadastrado
* @apiName typesMaterial
* @apiGroup TypeMaterial
*
* @apiSuccess {Object[]} materiais Lista de tipos de materiais.
*/

/**
* @api {put} /api/typeMaterial Alterar tipo de material cadastrado
* @apiName putTypeMaterial
* @apiGroup TypeMaterial
*
* @apiParam {Number} id ID do tipo de material.
* @apiParam {String} nome Nome do tipo de material.
*/

/**
* @api {del} /api/typeMaterial/:id Inativar tipo de material
* @apiName delTypeMaterial
* @apiGroup TypeMaterial
*
* @apiParam {Number} id ID do tipo de material.
*/

/**
* @api {post} /api/typeMaterial Inserção de tipo de material
* @apiName postTypeMaterial
* @apiGroup TypeMaterial
*
* @apiParam {String} nome Nome do tipo de material.
*/


// NOTE: MaterialPhoto

/**
* @api {get} /api/materialPhotos Buscar fotos dos materiais
* @apiName materialPhotos
* @apiGroup MaterialPhoto
*
* @apiSuccess {Object[]} materiais Lista de fotos.
*/

/**
* @api {put} /api/materialPhoto Alterar foto
* @apiName putMaterialPhoto
* @apiGroup MaterialPhoto
*
* @apiParam {Number} id ID da foto.
* @apiParam {Number} idMaterial ID do material.
* @apiParam {String} url URL da foto.
* @apiParam {Boolean} status Status desejável.
*/

/**
* @api {del} /api/materialPhoto/:id Inativar tipo de material
* @apiName delMaterialPhoto
* @apiGroup MaterialPhoto
*
* @apiParam {Number} id ID da foto do material.
*/

/**
* @api {post} /api/materialPhoto Inserção de tipo de material
* @apiName postMaterialPhoto
* @apiGroup MaterialPhoto
*
* @apiParam {Number} idMaterial ID do material.
* @apiParam {String} url URL da foto.
*/


// NOTE: INFO Material
/**
* @api {get} /api/material/infoMaterial/:id Buscar dados de um material
* @apiName infoMaterial
* @apiGroup Material
*
* @apiParam {Number} id ID do material.
*/
