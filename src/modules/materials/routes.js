import { get, post, del, put } from 'server/router';
import ctrl from './controller';

export default [
  get('/api/materials', ctrl.getListMaterials),
  post('/api/material', ctrl.insertMaterial),
  del('/api/material/:id', ctrl.deleteMaterial),
  put('/api/material', ctrl.updateMaterial),

  get('/api/typesMaterial', ctrl.getListMaterialType),
  post('/api/typeMaterial', ctrl.insertMaterialType),
  del('/api/typeMaterial/:id', ctrl.deleteMaterialType),
  put('/api/typeMaterial', ctrl.updateMaterialType),

  get('/api/materialPhotos', ctrl.getPhotoMaterial),
  get('/api/materialPhotos/:id', ctrl.getListPhotoMaterial),
  post('/api/materialPhoto', ctrl.insertMaterialPhoto),
  put('/api/materialPhoto', ctrl.updateMaterialPhoto),
  del('/api/materialPhoto/:id', ctrl.deleteMaterialPhoto),

  get('/api/material/infoMaterial/:id', ctrl.getInfoMaterial),
];
