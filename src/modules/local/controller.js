import { query } from '../../database';
import { GOOGLE_KEY } from '../../config';
import fetch from 'node-fetch';
import server from 'server';

const { status } = server.reply;

const getBairros = () => {
  return query('select * from bairros where status = $1', ['S']);
};

const insertBairro = ({ data }) => {
  const { nome, idCidade } = data;
  return query('insert into bairros(nome, id_cidade) values($1, $2) RETURNING id', [nome, idCidade]);
};

const deleteBairro = (ctx) => {
  const { id } = ctx.params;
  return query('update bairros set status = $2 where id = $1', [id, 'N']);
};

const updateBairro = (ctx) => {
  const { id, nome, idCidade } = ctx.data;
  return query('update bairros set nome = $1, id_cidade = $2 where id = $3', [nome, idCidade, id]);
};

const getCidades = () => {
  return query('select * from cidades where status = $1', ['S']);
};

const insertCidade = ({ data }) => {
  const { nome, idEstado } = data;
  return query('insert into cidades(nome, id_estado) values($1, $2) RETURNING id', [nome, idEstado]);
};

const deleteCidade = (ctx) => {
  const { id } = ctx.params;
  return query('update cidades set status = $2 where id = $1', [id, 'N']);
};

const updateCidade = (ctx) => {
  const { id, nome, idEstado } = ctx.data;
  return query('update cidades set nome = $1, id_estado = $2 where id = $3', [nome, idEstado, id]);
};

const getEstados = () => {
  return query('select * from estados where status = $1', ['S']);
};

const insertEstado = ({ data }) => {
  const { nome, nomeReduzido, idPais } = data;
  return query('insert into estados(nome, id_pais, nome_reduzido) values($1, $2, $3) RETURNING id', [nome, idPais, nomeReduzido]);
};

const deleteEstado = (ctx) => {
  const { id } = ctx.params;
  return query('update estados set status = $2 where id = $1', [id, 'N']);
};

const updateEstado = (ctx) => {
  const { id, nome, nomeReduzido, idPais } = ctx.data;
  return query('update estados set nome = $1, id_pais = $2, nome_reduzido = $4 where id = $3', [nome, idPais, id, nomeReduzido]);
};

const insertPais = ({ data }) => {
  const { nome, nomeReduzido } = data;
  return query('insert into paises(nome, nome_reduzido) values($1, $2) RETURNING id',[nome, nomeReduzido]);
};

const getLocal = (ctx) => {
  const { idBairro, idCidade, idEstado } = ctx.params;

  return query(`
    select *
      from estados
 left join cidades on (cidades.id_estado = estados.id and cidades.id = coalesce($2, cidades.id))
 left join bairros on (bairros.id_cidade = cidades.id and bairros.id = coalesce($3, bairros.id))
     where estados.id = coalesce($1, estados.id)
  `, [idEstado || null, idCidade || null, idBairro || null]);
};

const getLocalByNames = (endereco) => {
  const { pais, estado, cidade, bairro } = endereco;
  return query('select id from paises where paises.nome = $1', [pais.long_name.toUpperCase()]).then((retornoPaises) => {
    const idPais = getValueFromSelect(retornoPaises);

    if (idPais) {
      return query('select id from estados where id_pais = $1 and estados.nome = $2', [idPais, estado.long_name.toUpperCase()]).then((retornoEstados) => {
        const idEstado = getValueFromSelect(retornoEstados);

        if (idEstado) {
          return query('select id from cidades where id_estado = $1 and cidades.nome = $2', [idEstado, cidade.long_name.toUpperCase()]).then((retornoCidades) => {
            const idCidade = getValueFromSelect(retornoCidades);

            if (idCidade) {
              const nomeBairro = (bairro) ? bairro.long_name.toUpperCase() : cidade.long_name.toUpperCase();
              return query('select id from bairros where id_cidade = $1 and bairros.nome = $2', [idCidade, nomeBairro]).then((retornoBairros) => {
                const idBairro = getValueFromSelect(retornoBairros);

                if (idBairro) {
                  return idBairro;
                } else {
                  return insertLocais(endereco, idPais, idEstado, idCidade);
                }
              });
            } else {
              return insertLocais(endereco, idPais, idEstado);
            }
          });
        } else {
          return insertLocais(endereco, idPais);
        }
      });
    } else {
      return insertLocais(endereco);
    }
  });
};

const insertLocais = (endereco, idPais = false, idEstado = false, idCidade = false, idBairro = false) => {
  const { pais, estado, cidade, bairro } = endereco;

  if (idPais) {
    if (idEstado) {
      if (idCidade) {
        if (idBairro) {
          return idBairro;
        } else {
          const nomeBairro = (bairro) ? bairro.long_name.toUpperCase() : cidade.long_name.toUpperCase();
          return insertBairro({ data: { idCidade, nome: nomeBairro } }).then((retorno) => {
            const { id } = retorno[0];
            return insertLocais(endereco, idPais, idEstado, idCidade, id);
          });
        }
      } else {
        return insertCidade({ data: { idEstado, nome: cidade.long_name.toUpperCase() } }).then((retorno) => {
          const { id } = retorno[0];
          return insertLocais(endereco, idPais, idEstado, id);
        });
      }
    } else {
      return insertEstado({ data: { idPais, nome: estado.long_name.toUpperCase(), nomeReduzido: estado.short_name } }).then((retorno) => {
        const { id } = retorno[0];
        return insertLocais(endereco, idPais, id);
      });
    }
  } else {
    return insertPais({ data: { nome: pais.long_name.toUpperCase(), nomeReduzido: pais.short_name } }).then((retorno) => {
      const { id } = retorno[0];
      return insertLocais(endereco, id);
    });
  }
};

const getValueFromSelect = (rows) => {
  if ((rows) && (rows.length > 0) && (rows[0])) {
    return rows[0].id;
  }
  return false;
};

const getLocalizacao = (ctx) => {
  const { latitude, longitude } = ctx.params;

  const path = 'https://maps.googleapis.com/maps/api/geocode/json?&latlng=' + latitude + ',' + longitude + '&key=' + GOOGLE_KEY;

  return fetch(path, { method: 'GET' }).then((response) => {
    return response.text().then((retorno) => {
      if (retorno) {
        const { results } = JSON.parse(retorno);

        if ((results) && (results.length > 0)) {
          const local = results[0];

          const endereco = getLocalObject(local.address_components);

          return getLocalByNames(endereco).then((idBairro) => {
            return { idBairro };
          });
        } else {
          return status(409).send('Local não encontrado pela Google, verifique a latitude e longitude');
        }
      }
      return false;
    });
  });
};

const getLocalObject = (endereco) => ({
  pais: endereco.find((item) => item.types.find((type) => type === 'country')),
  estado: endereco.find((item) => item.types.find((type) => type === 'administrative_area_level_1')),
  cidade: endereco.find((item) => item.types.find((type) => type === 'administrative_area_level_2')),
  bairro: endereco.find((item) => item.types.find((type) => type === 'sublocality_level_1')),
});

export default {
  insertBairro,
  deleteBairro,
  updateBairro,

  insertCidade,
  updateCidade,
  deleteCidade,

  insertEstado,
  deleteEstado,
  updateEstado,

  getBairros,
  getCidades,
  getEstados,
  getLocal,

  getLocalizacao,
};
