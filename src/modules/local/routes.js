import { get, post, del, put } from 'server/router';
import ctrl from './controller';

export default [
  get('/api/bairros', ctrl.getBairros),
  post('/api/bairro', ctrl.insertBairro),
  del('/api/bairro/:id', ctrl.deleteBairro),
  put('/api/bairro', ctrl.updateBairro),

  get('/api/cidades', ctrl.getCidades),
  post('/api/cidade', ctrl.insertCidade),
  del('/api/cidade/:id', ctrl.deleteCidade),
  put('/api/cidade', ctrl.updateCidade),

  get('/api/estados', ctrl.getEstados),
  post('/api/estado', ctrl.insertEstado),
  put('/api/estado', ctrl.updateEstado),
  del('/api/estado/:id', ctrl.deleteEstado),

  get('/api/locais/:idEstado', ctrl.getLocal),
  get('/api/locais/:idEstado/:idCidade', ctrl.getLocal),
  get('/api/locais/:idEstado/:idCidade/:idBairro', ctrl.getLocal),

  get('/api/local/:latitude/:longitude', ctrl.getLocalizacao),
];
