import { get, post, del, put } from 'server/router';
import ctrl from './controller';

export default [
  get('/api/goal', ctrl.getGoal),
  post('/api/goal', ctrl.insertGoal),
  del('/api/goal/:id', ctrl.deleteGoal),
  put('/api/goal', ctrl.updateGoal),

  get('/api/v2/goal', ctrl.getGoalV2),
  post('/api/v2/goal', ctrl.insertGoalV2),
  del('/api/v2/goal/:id', ctrl.deleteGoalV2),
  put('/api/v2/goal', ctrl.updateGoalV2),

  get('/api/v2/goal/:idBairro', ctrl.getGoalByBairro)
];
