import server from 'server';
import { query } from '../../database';

const StatusReply = server.reply.status;

const getGoal = () => {
  return query(`
    select * from metas_pontuacao where status = 'S'
  `);
};

const insertGoal = (ctx) => {
  const { descricao, pontuacao, idRota, data } = ctx.data;
  return query('insert into metas_pontuacao(descricao, pontuacao, id_rota, data) values($1, $2, $3, $4)', [descricao, pontuacao, idRota, data]);
};

const deleteGoal = (ctx) => {
  const { id } = ctx.params;
  return query('update metas_pontuacao set status = $2 where id = $1', [id, 'N']);
};

const updateGoal = (ctx) => {
  const { id, descricao, pontuacao, idRota, data } = ctx.data;
  let { status } = ctx.data;

  if (!status) {
    status = 'S';
  }
  return query('update metas_pontuacao set descricao = $1, pontuacao = $2, id_rota = $3, data = $4, status = $6 where id = $5', [descricao, pontuacao, idRota, data, id, status]);
};

const getGoalV2 = () => {
  return query(`
    select * from goal where status = 'S'
  `);
};

const insertGoalV2 = (ctx) => {
  const { descricao, pontuacao, idBairro, data } = ctx.data;
  return query('insert into goal(descricao, pontuacao, id_bairro, data) values($1, $2, $3, $4)', [descricao, pontuacao, idBairro, data]);
};

const deleteGoalV2 = (ctx) => {
  const { id } = ctx.params;
  return query('update goal set status = $2 where id = $1', [id, 'N']);
};

const updateGoalV2 = (ctx) => {
  const { id, descricao, pontuacao, idBairro, data } = ctx.data;
  let { status } = ctx.data;

  if (!status) {
    status = 'S';
  }
  return query('update goal set descricao = $1, pontuacao = $2, id_bairro = $3, data = $4, status = $6 where id = $5', [descricao, pontuacao, idBairro, data, id, status]);
};

const getGoalByBairro = (ctx) => {
  const { idBairro } = ctx.params;

  return query(`
    select *
      from goal
     where extract('month' from goal.data) = extract('month' from current_date) and
           extract('year' from goal.data) = extract('year' from current_date) and
           id_bairro = $1
  `, [idBairro]).then((retorno) => (retorno.length > 0) ? retorno[0] : StatusReply(409).send('Meta não encontrada.'));
};

export default {
  getGoal,
  insertGoal,
  deleteGoal,
  updateGoal,

  getGoalV2,
  insertGoalV2,
  deleteGoalV2,
  updateGoalV2,

  getGoalByBairro,
};
