import { query } from '../../database';

const getRotas = (ctx) => {
  const { idBairro, idCidade, idEstado, idPeriodo } = ctx.params;

  return query(`
      select bairros.id as id
        from bairros
   left join cidades on (cidades.id = bairros.id_cidade and cidades.id = coalesce($2, cidades.id))
   left join estados on (estados.id = cidades.id_estado and estados.id = coalesce($3, estados.id))
       where bairros.id = coalesce($1, bairros.id)
  `, [idEstado, idCidade, idBairro]).then((bairros) => {
    const listBairros = bairros.map((item) => item.id).join(', ');
    return query(`
      select *
        from rotas
       where id_bairro in ($1) and
             status = 'S' and
             id_periodo = coalesce($2, id_periodo)
    `, [listBairros, idPeriodo]);
  });
};

const getRotasByBairro = (ctx) => {
  const { idBairro } = ctx.params;

  return query(`
    select * from rotas where id_bairro = $1 and status = 'S'
  `, [idBairro]);
};

const insertRota = (ctx) => {
  const { diaSemana, idBairro, periodo } = ctx.data;
  return query('insert into rotas(dia_semana, id_bairro, periodo) values($1, $2, $3)', [diaSemana, idBairro, periodo]);
};

const deleteRota = (ctx) => {
  const { id } = ctx.params;
  return query('update rotas set status = $2 where id = $1', [id, 'N']);
};

const updateRota = (ctx) => {
  const { id, diaSemana, idBairro, periodo } = ctx.data;

  let { status } = ctx.data;

  if (!status) {
    status = 'S';
  }

  return query('update rotas set dia_semana = $2, id_bairro = $3, periodo = $4 where id = $1', [id, diaSemana, idBairro, periodo]);
};

export default {
  getRotas,
  insertRota,
  deleteRota,
  updateRota,

  getRotasByBairro,
};
