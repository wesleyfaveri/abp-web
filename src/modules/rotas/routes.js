import { get, post, del, put } from 'server/router';
import ctrl from './controller';

export default [
  get('/api/rotas/bairro/:idBairro', ctrl.getRotasByBairro),
  
  get('/api/rotas/:periodo', ctrl.getLocal),
  get('/api/rotas/:idEstado/:periodo', ctrl.getLocal),
  get('/api/rotas/:idEstado/:idCidade/:periodo', ctrl.getLocal),
  get('/api/rotas/:idEstado/:idCidade/:idBairro/:periodo', ctrl.getLocal),

  post('/api/rota', ctrl.insertRota),
  del('/api/rota/:id', ctrl.deleteRota),
  put('/api/rota', ctrl.updateRota),
];
