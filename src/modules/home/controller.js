import { render } from 'server/reply';

const alive = (ctx) => {
  return 'ok';
};

const home = (ctx) => {
  return render('main');
};

export default {
  home,
  alive,
};
