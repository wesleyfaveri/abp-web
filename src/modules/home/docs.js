/**
* @api {get} /api/alive Verificação status do servidor
* @apiName alive
* @apiGroup Home
*
* @apiSuccess  Boolean Identificador de status do servidor.
*/
