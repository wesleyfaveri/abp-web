import { query } from '../../database';

const getScore = (ctx) => {
  const { idBairro } = ctx.params;
  return query(`
    select *
      from pontuacoes
      join rotas on (pontuacoes.id_rota = rotas.id and rotas.id_bairro = $1 and rotas.status = 'S')
     where pontuacoes.status = 'S' and
           extract('month' from pontuacoes.data) = extract('month' from current_date) and
           extract('year' from pontuacoes.data) = extract('year' from current_date)
  `, [idBairro]);
};

const insertScore = (ctx) => {
  const { idRota, pontos, idUsuario, data, descricao } = ctx.data;
  return query('insert into pontuacoes(id_rota, valor, id_usuario, data, descricao) values($1, $2, $3, $4, $5)', [idRota, pontos, idUsuario, data, descricao]);
};

const deleteScore = (ctx) => {
  const { id } = ctx.params;
  return query('update pontuacoes set status = $2 where id = $1', [id, 'N']);
};

const updateScore = (ctx) => {
  const { id, idRota, pontos, idUsuario, data, descricao } = ctx.data;
  let { status } = ctx.data;

  if (!status) {
    status = 'S';
  }
  return query('update pontuacoes set id_rota = $2, valor = $3, id_usuario = $4, data = $5, descricao = $6, status = $7 where id = $1', [id, idRota, pontos, idUsuario, data, descricao, status]);
};

export default {
  getScore,
  insertScore,
  deleteScore,
  updateScore,
};
