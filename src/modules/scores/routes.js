import { get, post, del, put } from 'server/router';
import ctrl from './controller';

export default [
  get('/api/score/:idBairro', ctrl.getScore),
  post('/api/score', ctrl.insertScore),
  del('/api/score/:id', ctrl.deleteScore),
  put('/api/score', ctrl.updateScore),
];
