import server from 'server';
import { query } from '../../database';

const { status } = server.reply;

const getUsers = () => {
  return query('select usuarios.id, usuarios.login, usuarios.nome, usuarios.status  from usuarios');
};

const getActiveUsers = () => {
  return query(`
    select usuarios.id, usuarios.login, usuarios.nome  from usuarios where status = 'S'
  `);
};

const insertUser = (ctx) => {
  const { login, senha, nome } = ctx.data;
  return query('insert into usuarios(login, senha, nome) values($1, $2, $3)  ON CONFLICT (login) DO UPDATE SET status = $4', [login, senha, nome, 'S']);
};

const deleteUser = (ctx) => {
  const { id } = ctx.params;
  return query('update usuarios set status = $2 where id = $1', [id, 'N']);
};

const updateUser = (ctx) => {
  const { id, login, senha, nome } = ctx.data;

  return query('select * from usuarios where id <> $1 and login = $2', [id, login]).then((retorno) => {
    if (retorno.length === 0) {
      return query('update usuarios set login = $1, senha = $2, nome = $3 where id = $4', [login, senha, nome, id]);
    } else {
      return status(409).send('Login já utilizado');
    }
  });
};

const login = (ctx) => {
  const { login, senha } = ctx.data;

  return query('select * from usuarios where login = $1', [login]).then((retorno) => {
    if (retorno.length > 0) {
      const usuario = retorno[0];

      if (usuario.senha === senha) {
        return { login: usuario.login, nome: usuario.nome };
      } else {
        return status(409).send('Usuário ou senha incorreto');
      }
    } else {
      return status(409).send('Usuário não encontrado');
    }
  });
};

export default {
  getUsers,
  getActiveUsers,
  insertUser,
  deleteUser,
  updateUser,
  login,
};
