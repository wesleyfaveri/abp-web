import { get, post, del, put } from 'server/router';
import ctrl from './controller';

export default [
  get('/api/users', ctrl.getUsers),
  get('/api/usersActive', ctrl.getActiveUsers),
  post('/api/user', ctrl.insertUser),
  del('/api/user/:id', ctrl.deleteUser),
  put('/api/user', ctrl.updateUser),
  post('/api/login', ctrl.login),
];
