// NOTE: User
/**
* @api {get} /api/users Busca todos usuários
* @apiName users
* @apiGroup User
*
* @apiSuccess {Object[]} users Lista de usuários.
*/

/**
* @api {get} /api/usersActive Busca todos usuários ativos
* @apiName usersActive
* @apiGroup User
*
* @apiSuccess {Object[]} users Lista de usuários.
*/

/**
* @api {post} /api/user Insere usuário
* @apiName postUser
* @apiGroup User
*
* @apiParam {String} login Login do usuário.
* @apiParam {String} nome Nome do usuário.
* @apiParam {String} senha Senha do usuário.
*/

/**
* @api {del} /api/user/:id Inativa usuário
* @apiName delUser
* @apiGroup User
*
* @apiParam {Number} id ID do usuário.
*/

/**
* @api {put} /api/user Edita usuário
* @apiName putUser
* @apiGroup User
*
* @apiParam {String} id ID do usuário.
* @apiParam {String} login Login do usuário.
* @apiParam {String} nome Nome do usuário.
* @apiParam {String} senha Senha do usuário.
*/

/**
* @api {post} /api/login Login do usuário
* @apiName postLogin
* @apiGroup User
*
* @apiParam {String} login Login do usuário.
* @apiParam {String} senha Senha do usuário.
*/
