import { get } from 'server/router';
import ctrl from './controller';

export default [
  get('/api/sync/user/:id', ctrl.getDataUser),
  get('/api/sync/guest/:idRegion/:id', ctrl.getDataGuest),
];
