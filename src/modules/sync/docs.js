/**
* @api {get} /api/sync/guest/:id Sincronização de dados de usuário comum
* @apiName Get Data Guest
* @apiGroup Sync
*
* @apiParam {Number} idRegion ID da região do usuário.
* @apiParam {Number} id ID da ultima atualização do usuário.
*
* @apiSuccess {Object} materiais Lista de materiais reciclaveis.
* @apiSuccess {Number} idAtualizacao  ID correspondente da última atualização.
*/

/**
* @api {get} /api/sync/user/:idUsuario/:id Sincronização de dados de usuário
* @apiName Get Data User
* @apiGroup Sync
*
* @apiParam {Number} idUsuario ID do usuário.
* @apiParam {Number} id ID da ultima atualização do usuário.
*
* @apiSuccess {Object} materiais Lista de materiais reciclaveis.
* @apiSuccess {Number} idAtualizacao  ID correspondente da última atualização.
* @apiSuccess {Number} scores  Lista de pontuações cadastradas.
*/
