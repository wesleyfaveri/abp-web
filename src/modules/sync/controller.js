import { query } from '../../database';

const getDataUser = (ctx) => {
  const { id } = ctx.params;

  return query('select usuarios.id, usuarios.login, usuarios.status  from usuarios');
};

const getDataGuest = async (ctx) => {
  const { id, idRegion } = ctx.params;
  try {
    const retorno = await query('select * from txid_current() as xmin');
    const { xmin } = retorno[0];
    const materiais = await getMaterials(id);

    return {
      materiais,
      idAtualizacao: xmin,
    };
  } catch (err) {
    console.log(err);
  }
};

const getMaterials = (id, resolve) => {
  return query(`
    select materiais.*,
           tipos_material.nome as nome_tipo_material
      from materiais inner join tipos_material on (materiais.id_tipo = tipos_material.id and tipos_material.status = 'S')
     where materiais.status = 'S' and materiais.xmin::text::bigint > $1
  `, [id]).then((retornoMateriais) => {
    if (retornoMateriais.length > 0) {
      const idsMateriais = retornoMateriais.map((item) => item.id).join(', ');
      return query(`
        select fotos_material.*
        from fotos_material
        join materiais on (materiais.id = fotos_material.id and materiais.status = 'S')
        where fotos_material.status = 'S' and fotos_material.id_material in (${idsMateriais})
        `).then((retornoFotosMateriais) => {
        const fotosMaterial = getFormatedFotosMaterial(retornoFotosMateriais);

        return getFormatedMaterials(retornoMateriais).map((item) => ({
          ...item,
          fotos: fotosMaterial.filter((foto) => foto.idMaterial === item.id),
        }));
      });
    } else {
      return retornoMateriais;
    }
  });
};

const getFormatedMaterials = (materiais) => {
  return materiais.map((item) => ({
    ...item,
    idTipo: item.id_tipo,
    'id_tipo': undefined,
    nomeTipoMaterial: item.nome_tipo_material,
    'nome_tipo_material': undefined,
  }));
};

const getFormatedFotosMaterial = (fotos) => {
  return fotos.map((item) => ({
    ...item,
    idMaterial: item.id_material,
    'id_material': undefined,
  }));
};

export default {
  getDataUser,
  getDataGuest,
};
