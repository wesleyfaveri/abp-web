import home from './home/routes';
import materials from './materials/routes';
import scores from './scores/routes';
import users from './users/routes';
import goals from './goal/routes';
import sync from './sync/routes';
import local from './local/routes';
import rotas from './rotas/routes';

export default [
  ...home,
  ...materials,
  ...scores,
  ...users,
  ...goals,
  ...sync,
  ...local,
  ...rotas,
];
